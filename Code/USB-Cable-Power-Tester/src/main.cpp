#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h"
#include <Adafruit_INA219.h>

// TODO: write power testing routine that is activated by Boot button.

const uint8_t I2C_SDA_PIN = 22; //SDA;  // ESP32 WROOM32 i2c SDA Pin
const uint8_t I2C_SCL_PIN = 21; //SCL;  // ESP32 WROOM32 i2c SCL Pin
#define CableGood 12   // GPIO of the Green Cable Good LED
#define CableBad  14   // GPIO of cable bad LED
#define PowerLED  25   // GPIO of the Power LED
#define RunningLED  33   // GPIO of the Runnings LED
#define CableBypass  27   // GPIO of the cable bypass MOSFET
#define WAIT  100
#define SWRun    17    // SW4 - press = 0
#define SWBypass 16    // SW3 - press = 0
#define Load1    26   // Load 1 MOSFET - high = turn on
#define Load2    32   // Load 2 MOSFET - high = turn on
#define Load3    19   // Load 3 MOSFET - high = turn on
#define ONn     HIGH  // N-MOS HIGH=On
#define ONp     LOW   // P-MOS LOW=On
#define OFFn    LOW   // N-MOS LOW-Off
#define OFFp    HIGH  // P-MOS HIGH-Off
  float Res1 = 0.0;
  float Res2 = 0.0;
  float Res3 = 0.0;

const uint8_t DISPLAY_WIDTH = 128;  // OLED display pixel width
const uint8_t DISPLAY_HEIGHT = 64;  // OLED display pixel hight
#define OLED_DISPLAY true    // Enable OLED Display
#define OLED_DISPLAY_SSD1306  // OLED Display Type: SSD1306(OLED_DISPLAY_SSD1306) / SH1106(OLED_DISPLAY_SH1106), comment this line out to disable oled

SSD1306Wire  display(0x3c,I2C_SDA_PIN, I2C_SCL_PIN);
Adafruit_INA219 ina219a(0x44);   // PCB i2C address is 0x44
Adafruit_INA219 ina219b(0x40);   // PCB i2C address is 0x40
// pre-list some funcations ahead of use
float readCurrentA(void) ;
float readVoltageA(void);
void displayText(int, char*);


void OLED_Header() {
  char msg[18];
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 0, "Cable Test");
  display.drawLine(0, 12, DISPLAY_WIDTH, 12);
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  //display.setTextAlignment(TEXT_ALIGN_LEFT);
  //sprintf(msg, "V:%.1f   A:%.3f\0", readVoltage(),readCurrent()/100 );
  sprintf(msg, "V:%.1f  mA:%.0f", readVoltageA(),readCurrentA() );
  display.drawString(128, 0, msg);   // currently have a 0.1 sensor resistor in place, so divide by 100
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.display();
}

float readCurrentA(void) {
  // Read current from INA219.
  float current_mA = ina219a.getCurrent_mA() ;
  return current_mA;
}

float readVoltageA(void) {
  // Read voltage from INA219.
  float shuntvoltage = ina219a.getShuntVoltage_mV();
  float busvoltage = ina219a.getBusVoltage_V();
  float loadvoltage = busvoltage + (shuntvoltage / 999);
  return loadvoltage;
}

void displayText(int line, char *textline) {
  // test out the INA219 current/voltage sensor
  display.clear();
  OLED_Header();
  display.drawString(2, line * 12, textline); 
  delay(100);
  display.display();
}

float truncate(float val, byte dec)
// truncate a float value to dec number of digits
{
    float x = val * pow(10, dec);
    float y = round(x);
    float z = x - y;
    if ((int)z == 5)
    {
        y++;
    } else {}
    x = y / pow(10, dec);
    return x;
}

void blinkUserLED() {
  // blink the User LEDs, Red, then Green
digitalWrite(CableBad, HIGH);
digitalWrite(CableGood, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(CableBad, LOW);
digitalWrite(CableGood, LOW);
}

void blinkUserLEDGreen() {
  // blink both the Green User LEDs
digitalWrite(CableGood, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(CableGood, LOW);
}

void blinkUserLEDRed() {
  // blink both the Green User LEDs
digitalWrite(CableBad, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(CableBad, LOW);
}

void measureINA219a() {
  // test out the INA219a - is it responding at i2C address 0x44, with voltage and current
  float amps = readCurrentA();
  float volts = readVoltageA();
  char reg1[10];
  char reg2[10];
  sprintf(reg1, "mA: %0.1f",amps );
  sprintf(reg2, "V: %0.1f",volts);
  displayText(3, reg2);
  delay(200);
  displayText(2, reg1);
  delay(200);
  Serial.println(String(ina219a.getBusVoltage_V()));
  Serial.println(String(ina219a.getShuntVoltage_mV()));
  Serial.println(String(ina219a.getCurrent_mA()));
  delay(200);
  //Serial.println(String(readVoltage()));
  /*
  Serial.print("Volts:");
  Serial.println(reg2);
  Serial.print("mA:");
  Serial.println(reg1);
*/
}

float takeMeasurments() {
  float CObv = 0.0;
  float COsv = 0.0;
  float COi = 0.0;
  float CIbv = 0.0;
  float CIsv = 0.0;
  float CIi = 0.0;
  float Volts = 0.0;
  float resistance = 0.1;
  char reg1[12];

  CObv = ina219a.getBusVoltage_V();
  COsv = ina219a.getShuntVoltage_mV();
  COi = ina219a.getCurrent_mA();
  CIbv = ina219b.getBusVoltage_V();
  CIsv = ina219b.getShuntVoltage_mV();
  CIi = ina219b.getCurrent_mA();
  Volts = CObv - CIbv + (CIsv/1000);
  resistance = Volts / (CIi / 1000);
  Serial.print(CObv);
  Serial.print(",");
  Serial.print(COsv);
  Serial.print(",");
  Serial.print(COi);
  Serial.print(",");
  Serial.print(CIbv);
  Serial.print(",");
  Serial.print(CIsv);
  Serial.print(",");
  Serial.print(CIi);
  Serial.print(",");
  Serial.print(Volts);
  Serial.print(",");
  Serial.println(resistance);
  sprintf(reg1, "%0.3f ohms",resistance );
  displayText(1,reg1);
  return resistance;
}


void setup() {
  // put your setup code here, to run once:o
  // Setup ESP32 GPIOs:
  pinMode(CableGood, OUTPUT);
  pinMode(CableBad, OUTPUT);
  pinMode(PowerLED, OUTPUT);
  pinMode(RunningLED, OUTPUT);
  pinMode(Load1, OUTPUT);
  pinMode(Load2, OUTPUT);
  pinMode(Load3, OUTPUT);
  pinMode(CableBypass, OUTPUT);
  pinMode(SWBypass, INPUT);
  pinMode(SWRun, INPUT);
  // turn off Loads 1, 2 & 3
  digitalWrite(Load1, OFFn);
  digitalWrite(Load2, OFFn);
  digitalWrite(Load3, OFFn);
  digitalWrite(CableBypass, OFFn);

  Serial.begin(115200);
  Wire.begin(22,21);   // (pin_SDA, pin_SLC)

  #if OLED_DISPLAY
    display.init();
    display.flipScreenVertically();   // setup depends on how the PCB is mounted
    display.setContrast(255);
    display.setFont(ArialMT_Plain_10);
    display.setColor(WHITE);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "Cable Test");
    display.drawLine(0, 12, DISPLAY_WIDTH, 12);
    display.display();
  #endif

  if (! ina219a.begin()) {
    Serial.println("Failed to find INA219a chip at 0x44 !!!!!");
    //while (1) { delay(10); }
  }
  if (! ina219b.begin()) {
    Serial.println("Failed to find INA219b chip at 0x40 !!!!!");
    //while (1) { delay(10); }
  }
  digitalWrite(PowerLED, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  int MaxCount = 20;
  float AddRes1 = 0.1;
  float AddRes2 = 0.1;
  float AddRes3 = 0.1;
  char reg1[12];
  char reg2[12];
  char reg3[12];
  blinkUserLED();
  // test for Cable Test switch has been pressed
  if (digitalRead(SWRun) == LOW) {
   //__asm__("nop\n\t");  
  // if cable Run switch is pressed, go turn on Load 1
  Serial.println("Cable Measurement Routine");
  digitalWrite(RunningLED, HIGH);
  digitalWrite(Load1, ONn);
  delay(200);
  Serial.println("Load1 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes1 = AddRes1 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load1, OFFn);
  // turn on Load 2
  digitalWrite(Load2, ONn);
  delay(100);
  Serial.println("Load2 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes2 = AddRes2 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load2, OFFn);
  // turn on Load 3
  digitalWrite(Load3, ONn);
  delay(100);
  Serial.println("Load3 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes3 = AddRes3 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load3, OFFn);
  // display data on OLED screen
  digitalWrite(RunningLED, LOW);
  Res1 = AddRes1/MaxCount;
  Res2 = AddRes2/MaxCount;
  Res3 = AddRes3/MaxCount;
  delay(300);
  } else {
   Serial.println("Waiting");
  delay(100);
  }
  // test for Cable Test switch has been pressed
  if (digitalRead(SWBypass) == LOW) {
  // if cable Run switch is pressed, go turn on Load 1
  Serial.println("Cable Bypass Routine");
  digitalWrite(RunningLED, HIGH);
  digitalWrite(CableBypass, ONn);
  digitalWrite(Load1, ONn);
  delay(200);
    Serial.println("Load1 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes1 = AddRes1 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load1, OFFn);
  // turn on Load 2
  digitalWrite(Load2, ONn);
  delay(100);
  Serial.println("Load2 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes2 = AddRes2 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load2, OFFn);
  // turn on Load 3
  digitalWrite(Load3, ONn);
  delay(100);
  Serial.println("Load3 On:");
  for ( int i=0; i < MaxCount; i++) {
    AddRes3 = AddRes3 + takeMeasurments();
    delay(100);
  }
  digitalWrite(Load3, OFFn);
  // turn off Loads 1, 2 & 3
  digitalWrite(RunningLED, LOW);
  digitalWrite(CableBypass, OFFn);
  Res1 = AddRes1/MaxCount;
  Res2 = AddRes2/MaxCount;
  Res3 = AddRes3/MaxCount;
  delay(300);
  } 
  sprintf(reg1, "%0.3f ohms",Res1);
  sprintf(reg2, "%0.3f ohms",Res2);
  sprintf(reg3, "%0.3f ohms",Res3);
    display.clear();
  OLED_Header();
  display.drawString(2, 1 * 12, reg1); 
  display.drawString(2, 2 * 12, reg2); 
  display.drawString(2, 3 * 12, reg3); 
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  display.drawString(128, 4 * 12, "Press Button-->"); 
  display.display();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  delay(300);
}
