# USB Cable Power Tester 
  
I was getting frustrated in not being able to buy a good quality USB cable while in Shenzhen, China market places.  This board will measure the resistance of the GND and VBUS lines and indicate if there are data wires present.
  
For more documentation visit: [https://gitlab.com/parkview/USB-Cable-Power-Tester](https://gitlab.com/parkview/USB-Cable-Power-Tester)  
You can read a bit more of the project's history here: [https://forum.swmakers.org/viewtopic.php?f=9&t=2452](https://forum.swmakers.org/viewtopic.php?f=9&t=2452)
  
## Hardware Features:
---------
#### Version 0.7 Features: 

* ESP32-WROOM microcontroller
* powered via USB Micro or USB C based power supplies 
* tests USB Micro and Apple Lightning iPhone cables
* will test the USB cable under three different loads
* could be used to test and measure USB Power Bank capacities
* has a built in test function
* results are displayed on i2C SSD1306 128x64px OLED screen 
  

## Available Software 
---------------
  
#### Arduino: 
* `USB-Cable-Power-Tester` - PlatformIO Arduino based code that does cable testing   
