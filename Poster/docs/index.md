# USB Cable Power Tester 
  
I was getting frustrated in not being able to buy a good quality USB cable while in Shenzhen, China market places.  This board will measure the resistance of the GND and VBUS lines and indicate if there are data wires present.
  
For more documentation visit [https://gitlab.com/parkview/USB-Cable-Power-Tester](https://gitlab.com/parkview/USB-Cable-Power-Tester)
  
## Hardware Features:
---------
#### Version 0.7 Features: 

* ESP32-WROOM microcontroller
* powered via USB Micro or USB C based power supplies
* tests USB Micro and Apple Lightning iPhone cables
* will test the USB cable under three different loads
* has a built in test function
* results are displayed on i2C SSD1306 128x64px OLED screen
  

## Available Software 
---------------
![Gitlab URL](images/USB-Cable-Power-Tester-QR_URL.png){ align=right : style="height:100px;width:100px"}
#### Arduino:
* `USB-Cable-Power-Tester`  PlatformIO Arduino based code that does cable testing

![RGB Controller V0.8](images/Basic_1-Port_Controller-Annotated.png){ align=left : style="height:350px;width:400px"}  
